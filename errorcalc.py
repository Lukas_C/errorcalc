#!/usr/bin/env python3
import sympy as sy
import numpy as np

from sympy.parsing.sympy_parser import parse_expr


def errorcalc(formula, val_dict, err_dict, warnings=True, debug=False, format_infix=r" \pm "):
    if debug:
        print("Dicts:", val_dict, err_dict)

    expr = parse_expr(formula)
    if debug:
        print("Parsed formula:", expr)

    # create formula for calculation
    expr_err = 0    # holds the formula for calculation
    sym_dict = {}
    for key in val_dict.keys():
        sym = sy.symbols(key)
        sym_err = sy.symbols(key+"Err")
        # save associated symbols
        sym_dict[key] = [sym, sym_err]

        expr_diff = sy.diff(expr, sym)

        expr_err += expr_diff**2 * sym_err**2

    expr_err = sy.sqrt(expr_err)
    if debug:
        print("Sym dict:", sym_dict)
        print("Expression:", expr_err)

    # substitute and evaluate
    if debug:
        print("Results:")
    res_list = []
    err_list = []
    # TODO: Dangerous! If the lengths do not match all, this produces undefined behaviour!
    if isinstance(val_dict[key], np.ndarray):
        length = len(val_dict[list(val_dict)[0]])
    else:
        length = 1

    for i in range(length):
        subs_list_val = []
        subs_list_err = []

        for key in val_dict.keys():
            # sanity checks
            val = val_dict[key]
            if isinstance(val, np.ndarray):
                val = val[i]
            else:
                if warnings:
                    print(f"Warning: The VALUES of {key} were detected to not be a numpy array. Treating as a constant number.")

            err = err_dict[key]
            if isinstance(err, np.ndarray):
                err = err[i]
            else:
                if warnings:
                    print(f"Warning: The ERRORS of {key} were detected to not be a numpy array. Treating as a constant number.")

            subs_list_val.append((sym_dict[key][0], val))
            subs_list_err.append((sym_dict[key][1], err))

        res = float(expr.subs(subs_list_val))
        err = float(expr_err.subs(subs_list_val).subs(subs_list_err))
        print(f"{res:.8f}" + format_infix + f"{err:.8f}")
        res_list.append(res)
        err_list.append(err)

    if length == 1:
        return res_list[0], err_list[0]
    else:
        return res_list, err_list




# testing
if __name__ == "__main__":
    formula = "m*a**2"

    val_dict = {
        "m": np.array([1, 2, 3], dtype=float),
        "a": np.array([4, 5, 6], dtype=float)
    }
    err_dict = {
        "m": np.array([0.1, 0.1, 0.1]),
        "a": np.array([0.5, 0.5, 0.5])
    }

    errorcalc(formula, val_dict, err_dict)
